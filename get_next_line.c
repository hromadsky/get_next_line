/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dhromads <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 15:36:54 by dhromads          #+#    #+#             */
/*   Updated: 2018/05/19 15:36:57 by dhromads         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static	int	detect_line(char **line, char **saves)
{
	int		i;
	char	*tmp;

	i = 0;
	tmp = *saves;
	while ((*saves)[i] != '\n')
	{
		if ((*saves)[i] == '\0')
			return (0);
		i++;
	}
	tmp[i] = '\0';
	*line = ft_strdup(tmp);
	*saves = ft_strdup(&tmp[i + 1]);
	free(tmp);
	return (1);
}

static	int	reader(char *buff, char **line, char **saves, int fd)
{
	int		val;
	char	*tmp;

	while ((val = read(fd, buff, BUFF_SIZE)))
	{
		buff[val] = '\0';
		if (*saves != NULL)
		{
			if (!(tmp = ft_strjoin(*saves, buff)))
				return (-1);
			free(*saves);
			if (!(*saves = ft_strdup(tmp)))
				return (-1);
			free(tmp);
		}
		else
		{
			if (!(*saves = ft_strdup(buff)))
				return (-1);
		}
		if (detect_line(line, saves))
			break ;
	}
	return (val);
}

int			get_next_line(const int fd, char **line)
{
	static char	*saves[1025];
	char		buf[BUFF_SIZE + 1];
	int			val;

	if (fd < 0 || fd > 1025 || !line || (read(fd, saves[fd], 0) < 0))
		return (-1);
	if (saves[fd])
		if (detect_line(line, &saves[fd]))
			return (1);
	val = reader(buf, line, &saves[fd], fd);
	if (val || saves[fd] == NULL || saves[fd][0] == '\0')
	{
		if (!val && *line)
			*line = NULL;
		if (val > 0)
			return (1);
		else
			return (val);
	}
	*line = saves[fd];
	saves[fd] = NULL;
	return (1);
}
